import React from "react";

import "./Menu-item.css";

function MenuItem({ icon, text}) {
    // console.log(icon, text);
	return (
		<>
			<div className="menu-item">
				<div className="item-img">
					<img src={icon} alt=""></img>
					<div className="item-txt">
						<a href="#">{text}</a>
					</div>
				</div>
			</div>
		</>
	);
}

export default MenuItem