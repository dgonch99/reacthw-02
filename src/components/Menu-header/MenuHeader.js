import React from "react";

import "./MenuHeader.css";

function MenuHeader() {
    return (
			<>
				<div className="menu-header">
					<div className="logo">AF</div>
					<div className="menu-name">
						Animated Fred
						<span>
							<a href="mailto:animated@demo.com">animated@demo.com</a>
						</span>
					</div>
					<div className="menu-title"></div>
				</div>
			</>
		);
}

export default MenuHeader;