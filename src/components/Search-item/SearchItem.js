import React from "react";
import search from "../../icons/search.svg";

import "./Search-item.css"

function SearchItem() {
	
	return (
		<>
			<div className="search-item">
				<div className="img">
					<img src={search} alt="search"></img>
					<input type="text" className="search-input" placeholder="Search"></input>
				</div>
			</div>
		</>
	);
}

export default SearchItem;
