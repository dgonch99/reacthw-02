import React from "react";

import sun from "../../icons/sun.svg";
import "./modeswitch.css";


function ModeSwitch() {
    return (
        <>
        <div className="switch-item">
                <div className="switch-pin">
                    <img src={sun} alt="dark">

                    </img>
                    
        </div>
        </div>
        
        </>
    )
}

export default ModeSwitch;