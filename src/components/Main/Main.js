import React from "react";
import MenuItem from "../Menu-item/MenuItem";
import MenuHeader from "../Menu-header/MenuHeader";
import SearchItem from "../Search-item/SearchItem";
import ModeSwitch from "../ModeSwitch/ModeSwitch";

import home from "../../icons/home.svg";
import bar from "../../icons/bar.svg";
import bell from "../../icons/bell.svg";
import pie from "../../icons/pie.svg";
import packag from "../../icons/package.svg";
import logout from "../../icons/logout.svg";
import moon from "../../icons/moon.svg";

import "./main.css";


function Main() {
    return (
			// <>
				<div className="round">
					<div className="main-menu">
						<div className="menu-part1">
							<MenuHeader></MenuHeader>
							<SearchItem></SearchItem>
							<MenuItem icon={home} text="Dashboard"></MenuItem>
							<MenuItem icon={bar} text="Revenue"></MenuItem>
							<MenuItem icon={bell} text="Notifications"></MenuItem>
							<MenuItem icon={pie} text="Analytics"></MenuItem>
							<MenuItem icon={packag} text="Inventory"></MenuItem>
						</div>
						<div className="menu-part2">
							<MenuItem icon={logout} text="Log out"></MenuItem>
							<div className="darkmode-item">
								<MenuItem icon={moon} text="Darkmode"></MenuItem> <ModeSwitch></ModeSwitch>
							</div>
						</div>
					</div>
				</div>
			// </>
		);
}

export default Main